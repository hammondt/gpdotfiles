alias k='kubectl'
alias tf='terraform'
alias e4usa='gp idp login aws --role-arn ${ROLE_E4USA}'
alias dev='gp idp login aws --role-arn ${ROLE_DEV}'
alias prod='gp idp login aws --role-arn ${ROLE_PROD_IT}'
alias dashboards='gp idp login aws --role-arn ${ROLE_DASHBOARDS}'
alias prairielearn='gp idp login aws --role-arn ${ROLE_PRAIRIELEARN}'
source /home/linuxbrew/.linuxbrew/etc/bash_completion.d/kubectl
source /home/linuxbrew/.linuxbrew/etc/bash_completion.d/k9s
source /home/linuxbrew/.linuxbrew/etc/bash_completion.d/brew
source /home/linuxbrew/.linuxbrew/etc/bash_completion.d/argocd
source /home/linuxbrew/.linuxbrew/etc/bash_completion.d/yq
